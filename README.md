_*System tested with SqLite._

_*Some comments added to necessary functions._

## Question 1

**Defined URLs**

    # adds dummy data to DB
    # gets "count" as parameter represents number of necessary dummy data to add
    /question1/addDummy
    # deletes all dummy data from DB also resets PK counter
    /question1/deleteAll
    # shows results within 48 hours
    /question1/show

**Explanation**

Necessary function can be seen under [Question1's View File](./question1/views.py), function name is _get_results_.

_get_result_ function only sends 1 query and performance is optimized. Installing or designing some DB caching framework
and implementing is a possible solution to increase performance. It can improve performance by getting already loaded
data from memory instead of DB.

## Question 2

**Models**

To meet the requirements, _many-to-many relationship_ defined between _Bin_ and _Operation_ tables.
_collection_frequency_ and _last_collection_ fields also added to connection table because they can be meaningful only
if both _Bin_ and _Operation_ instances exist.

__Entity-Relationship Diagram can be seen below.__

![alt text](./question2/er.jpg)

**Defined URLs**

    # adds dummy data to DB
    /question2/addDummy
    # deletes all dummy data from DB also resets PK counter
    /question2/deleteAll
    # shows all results
    /question2/show

Necessary function can be seen under [Question2's View File](./question2/views.py), function name is _get_results_.

