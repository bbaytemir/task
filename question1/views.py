from datetime import datetime, timedelta
from random import randint

from django.db import connection
from django.db.models import Max, F, Func, Value, CharField
from django.http import HttpResponse

# Create your views here.
from question1.models import Vehicle, NavigationRecord


def home_view(request):
    return HttpResponse("Question1")


# uses bulk create to improve performance
def add_dummy_view(request):
    count = 1
    # needed vehicle instance count
    if request.GET.get("count"):
        count = int(request.GET["count"])

    # to use bulk create gets last id from DB
    last_id = 0
    try:
        last_id = Vehicle.objects.latest("id").id
    except Vehicle.DoesNotExist:
        # DB doesn't contain any vehicle instance
        print("no value inside DB")

    vehicles = []
    for i in range(count):
        # generates unique plate
        plate = "06 AV 123" + str(last_id + i)
        vehicles.append(Vehicle(plate=plate))

    Vehicle.objects.bulk_create(vehicles)

    navigation_records = []
    # adds 5 navigation record to DB for each newly added vehicle instance
    for vehicle_id in range(last_id + 1, last_id + count + 1):
        for i in range(5):
            # to generate more accurate dummy data
            # navigation record generated with random day subtraction
            navigation_records.append(
                NavigationRecord(longitude=12.22,
                                 latitude=33.33,
                                 dateTime=datetime.now() - timedelta(days=randint(0, 10)),
                                 vehicle_id=vehicle_id)
            )

    NavigationRecord.objects.bulk_create(navigation_records)

    return HttpResponse('Added {0} vehicle instance.'.format(count))


# deletes all instances
# NavigationRecord table's delete operation selected as cascade
# so deleting from Vehicle reflects to other table
def delete_all_view(request):
    # removed all instances from DB
    Vehicle.objects.all().delete()

    # added for resetting PK counter
    # newly added instances starts with PK=1
    cursor = connection.cursor()
    sql = 'UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME="{0}"'
    truncate_1 = sql.format(Vehicle._meta.db_table)
    truncate_2 = sql.format(NavigationRecord._meta.db_table)
    # print(truncate_1)
    cursor.execute(truncate_1)
    # print(truncate_2)
    cursor.execute(truncate_2)

    return HttpResponse("All instances removed, PK counter is 1.")


def show_view(request):
    results = get_results()

    return HttpResponse(str(results))


# function returns result which is formatted as shown inside attachment
# to increase performance only 1 query sent to DB
def get_results():
    # first we use filter to get only necessary data from DB to increase performance
    results = NavigationRecord.objects.filter(dateTime__gte=datetime.now() - timedelta(days=2)) \
        .values(
        # selects column to use in group by state and renames field to wanted name
        vehicle_plate=F("vehicle__plate")
    ) \
        .annotate(
        # gets maximum date time in DB after group by plate
        # STRFTIME function used for converting Sqlite DateTimeField to formatted string
        datetime=Func(
            Value('%d.%m.%Y %H:%M:%S'),
            Max('dateTime'),
            function='STRFTIME',  # works for SqlLite
            output_field=CharField()
        ),
        # adds latitude and longitude fields to result
        latitude=F("latitude"),
        longitude=F("longitude")
    )
    # added for see the resulting query
    print(results.query)
    # returns result QuerySet as result list
    return list(results)
