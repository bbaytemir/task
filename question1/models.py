from django.db import models

# Create your models here.
from django.db.models import DateTimeField, FloatField, CharField


class Vehicle(models.Model):
    plate = CharField(max_length=20)


class NavigationRecord(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    dateTime = DateTimeField()
    latitude = FloatField()
    longitude = FloatField()
