from django.urls import path

from .views import *

urlpatterns = [
    path('', home_view, name='index'),
    path('addDummy', add_dummy_view, name="add_dummy"),
    path('deleteAll', delete_all_view, name='delete_all'),
    path('show', show_view, name="show"),
]
