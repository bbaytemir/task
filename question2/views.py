from datetime import datetime, timedelta
from random import randint

from django.db import connection
from django.http import HttpResponse

from question2.models import BinOperation, Bin, Operation


def home_view(request):
    return HttpResponse("Question2")


def add_dummy_view(request):
    # truncates table
    delete_all_view(request)

    Operation(name="Operation 1").save()
    Operation(name="Operation 2").save()
    Operation(name="Operation 3").save()

    for i in range(1, 6):
        Bin(
            latitude=11.1 + (0.1 * i),
            longitude=21.12 + (0.1 * i)
        ).save()

        for j in range(1, 4):
            print("i:{0} - j:{1}".format(i, j))

            BinOperation(
                bin_id=i,
                operation_id=j,
                collection_frequency=2,
                last_collection=datetime.now() - timedelta(days=randint(0, 15))
            ).save()

    return HttpResponse('3 Operation instance added.<br>'
                        '5 Bin instance added.<br>'
                        '15 BinOperation instance added')


# deletes all instances
# Bin and Operation table's delete operation selected as cascade
# so deleting from them reflects to BinOperation table
def delete_all_view(request):
    # removed all instances from DB
    Bin.objects.all().delete()
    Operation.objects.all().delete()

    # added for resetting PK counter
    # newly added instances starts with PK=1
    cursor = connection.cursor()
    sql = 'UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME="{0}"'
    truncate_1 = sql.format(BinOperation._meta.db_table)
    truncate_2 = sql.format(Bin._meta.db_table)
    truncate_3 = sql.format(Operation._meta.db_table)
    # print(truncate_1)
    cursor.execute(truncate_1)
    # print(truncate_2)
    cursor.execute(truncate_2)
    # print(truncate_3)
    cursor.execute(truncate_3)

    return HttpResponse("All instances removed, PK counter is 1.")


def show_view(request):
    results = get_results()
    # result doesn't show objects but they accessible
    return HttpResponse(str(results))


# function returns result i couldn't understand the return format
# so my best guess was to return frequency and instances of bin and operation objects with that
# to increase performance only 1 query sent to DB to achieve this used select_related
def get_results():
    # gets all results from BinOperation table then
    bin_operation_list = BinOperation.objects.select_related("bin", "operation")
    print(bin_operation_list.query)
    # formats to query set
    results = [{
        "collection_frequency": binOperation.collection_frequency,
        "bin": binOperation.bin,
        "operation": binOperation.operation
    } for binOperation in bin_operation_list]

    return list(results)
