from django.db import models

# Create your models here.
from django.db.models import FloatField, CharField, IntegerField, DateTimeField


class Bin(models.Model):
    latitude = FloatField()
    longitude = FloatField()


class Operation(models.Model):
    name = CharField(max_length=100)


class BinOperation(models.Model):
    bin = models.ForeignKey(Bin, on_delete=models.CASCADE)
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
    collection_frequency = IntegerField()
    last_collection = DateTimeField()
